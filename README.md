# NumpyScan
View DRRs and other images saved as Numpy arrays. Supports four-field box and
single views.

![Main Window](images/mainwin.png)

## Dependencies
(Minimum tested versions)  
Python >= 3.5  
PyQt >= 5.6.0  
Numpy >= 1.14.0  
Matplotlib >= 2.1.2

If you are using the Anaconda python distribution, you can load a pre-built
environment by opening a command prompt inside the provided conda folder, and
then running `conda env create -f environment.yml`

Then run `activate NumpyScan` to activate the python environment, and
`deactivate` to close.

## Running
Make sure all dependencies have been installed (see [above](#dependencies)).

Start with `python viewer.py`. Depending on your operating system and how
Python 3 was installed, you may instead need to run `python3 viewer.py`.

## All files
No matter if you want to view four-field or single-field images, the images
must be saved as numpy arrays and their file names must end with .npy.

Sometimes multiple .npy files may exist in the same directory - for example,
image and mask files. If all the image file names contain a certain word, you
can limit NumpyScan to only show those images with the "Keyword (optional)"
text field. Type in the keyword first before choosing scan type.

## Four-field DRR viewing when the fields are saved in separate files.
This program transverses multiple directories and attempts to match
anterior-posterior, posterior-anterior, and right-lateral images for each patient.
Because of this,file names for anterior-posterior view must contain
`_AP_`.  
Similarly, file names for posterior-anterior view must contain `_PA_`,
and right-lateral view contain `_RL_`.

There are no such requirements for single-field files.

Four-field files where the AP, PA, and Lateral views are saved as a nested array
within the same .npy file may similarly be named any way. However, the first
nested sub-array must be for the AP view, the second for the PA view, and the
third for the Lateral view. This corresponds to the "4-field Box, single file"
Scan Type option.

## Copying image files
Images that are saved to a single file can be copied to one of two
user-specified folders:
1. First make sure an image is loaded and displayed in the viewing field.
Whichever image is displayed is the one that will be copied.

2. Click the first "Copy to" button, and choose the folder into which you want
to copy the file.

3. If you want to copy to a second folder, click the second "Copy to" button and
follow the same steps as before.

4. Now, whenever you click either button, the loaded image will be copied to the
corresponding directory. There will also be a notification in your terminal or
command prompt that shows the file copy.

## License
NumpyScan is copyright (C) 2018 S. Gay and licensed under the terms of the MIT
license. See License.md for more details.
