import os
import sys
import unittest
sys.path.append('..')
import viewer


class TestFourField(unittest.TestCase):
    def setUp(self):
        self.target = viewer.fourField
        self.keyword = None

    def test_view_files_sorted_empty(self):
        """
        Ensure that an empty list is returned as the globbing in
        self.target.sortFiles should return empty lists
        """
        pathList = ['test_data', 'test_data', 'test_data']
        expect = []
        self.assertEqual(self.target.sortFiles(self, pathList, '.npy'), expect)

    def test_view_files_sorted(self):
        """
        Tests that data files from three separate locations are found, and only
        files that have the same basic name (minus view ids of AP, PA, and RL)
        are returned
        """
        pathList = ['test_data/AP', 'test_data/PA', 'test_data/RL']
        pathList = [
            os.path.abspath(os.path.join(os.getcwd(), path))
            for path in pathList
        ]
        expect = ['demo1', 'demo2']
        self.assertEqual(self.target.sortFiles(self, pathList, '.npy'), expect)

    def test_view_files_sorted_and_exclude_keyword(self):
        """
        Ensure that only data files that contain a certain keyword are
        returned
        """
        pathList = ['test_data/AP', 'test_data/PA', 'test_data/RL']
        pathList = [
            os.path.abspath(os.path.join(os.getcwd(), path))
            for path in pathList
        ]
        expect = ['demo2']
        self.keyword = 'mask'
        self.assertEqual(self.target.sortFiles(self, pathList, '.npy'), expect)
        self.keyword = None

    def breakDown(self):
        pass


class TestSingleField(unittest.TestCase):
    def setUp(self):
        self.target = viewer.singleField
        self.keyword = None

    def test_view_file_sorted_empty(self):
        """
        Ensure that an empty list is returned as the globbing in
        self.target.sortFiles should return empty lists
        """
        pathList = ['test_data']
        expect = []
        self.assertEqual(self.target.sortFiles(self, pathList, '.npy'), expect)

    def test_view_file_by_extension(self):
        """
        Ensure that only files with the correct extension are sorted.
        """
        pathList = ['test_data/AP']
        expect = ['demo1_AP_', 'demo2_AP_mask']
        self.assertEqual(self.target.sortFiles(self, pathList, '.npy'), expect)

    def test_filter_file_by_keyword(self):
        """
        Ensure that only files containing a certain keyword in their name are
        returned
        """
        self.keyword = 'mask'
        pathList = ['test_data/AP']
        expect = ['demo2_AP_mask']
        self.assertEqual(self.target.sortFiles(self, pathList, '.npy'), expect)
        self.keyword = None


if __name__ == '__main__':
    unittest.main()
