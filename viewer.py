"""
Easily view images of radio scans (CT, PET, MRI) that are saved as Numpy arrays.
"""

# Copyright (C) 2018 S. Gay and licensed under the MIT license
import ctypes
import glob
import os
from platform import platform, system
from shutil import copy
import sys
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
# from matplotlib.figure import Figure
import numpy as np

from PyQt5 import QtWidgets
from NSui import Ui_MainWindow


class Gui(QtWidgets.QMainWindow):
    """
    Handles GUI calls. As many non-UI specific processes, such as searching for
    numpy images or setting up the plot(s), are handled with other classes
    (based on image view type) as are possible.
    """

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.radioButton4Field.clicked.connect(self.findScanPaths)
        self.ui.radioButton4FieldSingle.clicked.connect(self.findScanPaths)
        self.ui.radioButton1Field.clicked.connect(self.findScanPaths)
        self.ui.pushButtonCopy1.clicked.connect(self.copy1)
        self.ui.pushButtonCopy2.clicked.connect(self.copy2)

        self.ui.listWidget.clicked.connect(self.showPlot)

        self.pwd = os.getcwd()
        self.pathList = []
        self.filePath = None
        self.cmap = 'gray'
        self.keyword = None
        self.copyDir1 = None
        self.copyDir2 = None

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        # self.canvas.setSizePolicy(QtWidgets.QSizePolicy.Minimum,
        #                           QtWidgets.QSizePolicy.Minimum)
        # self.canvas.updateGeometry()
        self.ui.layoutViewer.addWidget(self.canvas)

    def findScanPaths(self):
        """
        Find paths to scan(s)
        """
        # Reset if not first run
        if self.pathList:
            self.resetVars()
            print("Temporary variables cleared.")

        if self.ui.lineEditKeyword.text() != '':
            self.keyword = str(self.ui.lineEditKeyword.text())
            print('Files whose names include %s will be excluded.' %
                  self.keyword)

        if self.ui.radioButton4Field.isChecked():
            self.pathList = fourField.findPaths(self, self.pwd)
            shortList = fourField.sortFiles(self, self.pathList, '.npy')

        elif self.ui.radioButton4FieldSingle.isChecked():
            self.pathList = FourFieldSingleFile.findPaths(self, self.pwd)
            shortList = FourFieldSingleFile.sortFiles(self, self.pathList,
                                                      '.npy')

        elif self.ui.radioButton1Field.isChecked():
            self.pathList = singleField.findPaths(self, self.pwd)
            shortList = singleField.sortFiles(self, self.pathList, '.npy')

        self.ui.listWidget.addItems(shortList)

    def resetVars(self):
        """
        Reset variables and the UI so we don't have to restart every time we
        want to view new images.
        """
        self.figure.clear('all')
        self.canvas.draw()
        self.ui.listWidget.clear()
        self.ui.lineEditCopy1.clear()
        self.ui.lineEditCopy2.clear()
        self.ui.lineEditAP.clear()
        self.ui.lineEditPA.clear()
        self.ui.lineEditRL.clear()
        self.ui.lineEdit.clear()
        self.pathList = []
        self.filePath = None
        self.copyDir1 = None
        self.copyDir2 = None

    def showPlot(self):
        """
        Determine plot type (4-field or single view) and call appropriate
        function to display image(s).
        """
        baseName = self.ui.listWidget.currentItem().text()

        self.figure.clear('all')

        if not self.ui.checkBoxGrayScale.isChecked():
            self.cmap = None
        else:
            self.cmap = 'gray'

        if self.ui.radioButton4Field.isChecked():
            fourField.plot(self, baseName, '.npy')
        elif self.ui.radioButton4FieldSingle.isChecked():
            FourFieldSingleFile.plot(self, baseName, '.npy')
        elif self.ui.radioButton1Field.isChecked():
            singleField.plot(self, baseName, '.npy')

    def copy1(self):
        """
        Copies actively loaded image file to user-specified directory or folder.
        If target directory has not yet been specified, this prompts the user
        to navigate to it using the system file manager, and then displays the
        chosen path in the UI. This function is for the **first** copy option
        in the UI.
        """
        if self.ui.radioButton4Field.isChecked():
            print("Not yet implemented for multi-file image sets.")
            return

        if not self.copyDir1:
            msg = 'Select folder to copy images into.'
            self.copyDir1 = QtWidgets.QFileDialog.getExistingDirectory(
                self, msg, self.pwd)
            self.ui.lineEditCopy1.setText(self.copyDir1)
        copy(self.filePath, self.copyDir1)
        print('Copied %s to %s' % (self.filePath, self.copyDir1))

    def copy2(self):
        """
        Copies actively loaded image file to user-specified directory or folder.
        If target directory has not yet been specified, this prompts the user
        to navigate to it using the system file manager, and then displays the
        chosen path in the UI. This function is for the **second** copy option
        in the UI.
        """
        if self.ui.radioButton4Field.isChecked():
            print("Not yet implemented for multi-file image sets.")
            return

        if not self.copyDir2:
            msg = 'Select folder to copy images into.'
            self.copyDir2 = QtWidgets.QFileDialog.getExistingDirectory(
                self, msg, self.pwd)
            self.ui.lineEditCopy2.setText(self.copyDir2)
        copy(self.filePath, self.copyDir2)
        print('Copied %s to %s' % (self.filePath, self.copyDir2))


class fourField(Gui):
    """
    Handle operations for 4-field images. Note that only three images will
    be shown since Right-Left and Left-Right views are mirrors of each other.
    """

    def findPaths(self, pwd):
        """
        Get paths to Anterior-Posterior, Posterior-Anterior, and Right-Left
        view numpy arrays.
        :params self: requires a `self` object from the Gui class (for
        QFileDialog)
        :params pwd: present working directory
        returns pathList: a list of paths for AP, PA, and RL arrays
        """
        msg1 = 'Select AP image folder'
        msg2 = 'Select PA image folder'
        msg3 = 'Select RL image folder'

        path1 = QtWidgets.QFileDialog.getExistingDirectory(self, msg1, pwd)
        path2 = QtWidgets.QFileDialog.getExistingDirectory(self, msg2, pwd)
        path3 = QtWidgets.QFileDialog.getExistingDirectory(self, msg3, pwd)

        pathList = [path1, path2, path3]

        self.ui.lineEditAP.setText(pathList[0])
        self.ui.lineEditPA.setText(pathList[1])
        self.ui.lineEditRL.setText(pathList[2])

        return pathList

    def sortFiles(self, pathList, extension):
        """
        Search through the provided directories to find views for AP, PA, and
        RL images, then return images that occur in all three locations.
        :params pathList: a list whose 3 elements are the paths to the AP, PA,
        and RL images, respectively.
        :return commonList: a list that contains all patient images that occur
        in all AP, PA, and RL paths.
        """
        pathListAP = glob.glob(pathList[0] + '/*' + extension)
        pathListPA = glob.glob(pathList[1] + '/*' + extension)
        pathListRL = glob.glob(pathList[2] + '/*' + extension)

        # Filter out any that don't contain the optional keyword
        if self.keyword:
            pathListAP = [word for word in pathListAP if self.keyword in word]
            pathListPA = [word for word in pathListPA if self.keyword in word]
            pathListRL = [word for word in pathListRL if self.keyword in word]

        # Get a list of file names without paths, view tags (like '_AP_'),
        # or extension
        shortListAP = [
            os.path.split(name)[-1].split('_AP_')[0] for name in pathListAP
        ]
        shortListPA = [
            os.path.split(name)[-1].split('_PA_')[0] for name in pathListPA
        ]
        shortListRL = [
            os.path.split(name)[-1].split('_RL_')[0] for name in pathListRL
        ]

        # Find all images which occur in all three locations
        commonList = list(
            set(shortListAP).intersection(shortListPA).intersection(
                shortListRL))
        commonList.sort()

        return commonList

    def plot(self, baseName, extension):
        """
        Display AP, PA, and RL views in window.
        :params self: requires `self` object from Gui
        :params basename: file name ID (patient ID or other similar name that
        is shared among all files)
        """
        if self.keyword:
            pathAP = self.pathList[0] + '/' + baseName + '_AP_' + self.keyword + extension
            pathPA = self.pathList[1] + '/' + baseName + '_PA_' + self.keyword + extension
            pathRL = self.pathList[2] + '/' + baseName + '_RL_' + self.keyword + extension
        else:
            pathAP = self.pathList[0] + '/' + baseName + '_AP_img' + extension
            pathPA = self.pathList[1] + '/' + baseName + '_PA_img' + extension
            pathRL = self.pathList[2] + '/' + baseName + '_RL_img' + extension

        arrayAP = np.load(pathAP)
        arrayPA = np.load(pathPA)
        arrayRL = np.load(pathRL)

        apPlot = self.figure.add_subplot(1, 3, 1)
        plt.imshow(arrayAP, cmap=self.cmap)
        plt.axis('off')
        plt.tight_layout()
        apPlot.set_title('AP View')

        paPlot = self.figure.add_subplot(1, 3, 2)
        plt.imshow(arrayPA, cmap=self.cmap)
        plt.axis('off')
        plt.tight_layout()
        paPlot.set_title('PA View')

        latPlot = self.figure.add_subplot(1, 3, 3)
        plt.imshow(arrayRL, cmap=self.cmap)
        plt.axis('off')
        plt.tight_layout()
        latPlot.set_title('Lateral View')

        self.canvas.draw()


class singleField(Gui):
    """
    Handle operations for single-field images.
    """

    def findPaths(self, pwd):
        """
        Get path to images.
        :params self: requires a `self` object from the Gui class (for
        QFileDialog)
        :params pwd: present working directory
        returns pathList: a list of the path to the single view arrays
        """
        msg = 'Select image folder'

        path = QtWidgets.QFileDialog.getExistingDirectory(self, msg, pwd)
        pathList = [path]

        self.ui.lineEdit.setText(pathList[0])

        return pathList

    def sortFiles(self, pathList, extension):
        """
        Search through the single directory path provided, then find and sort
        in alphabetical order a list of all image files saved as numpy arrays.
        :params pathList: a list whose only element is the path to a single
        directory containing the numpy arrays
        :params shortListIM: a sorted list with names of all found numpy image
        arrays. Does not include path or extension.
        """
        pathListIM = glob.glob(pathList[0] + '/*' + extension)

        # Filter out any that don't contain the optional keyword
        if self.keyword:
            pathListIM = [word for word in pathListIM if self.keyword in word]

        # Get a list of file names without paths or extension
        shortListIM = [
            os.path.split(name)[-1].split(extension)[0] for name in pathListIM
        ]
        shortListIM.sort()

        return shortListIM

    def plot(self, baseName, extension):
        """
        Display image view in windows.
        :params self: requires `self` object from Gui
        :params basename: file name ID (patient ID or other similar name that
        is shared among all files)
        """
        self.filePath = self.pathList[0] + '/' + baseName + extension

        arrayIM = np.load(self.filePath)

        imgPlot = self.figure.add_subplot(111)
        plt.imshow(arrayIM, cmap=self.cmap)
        plt.axis('off')
        plt.tight_layout()
        imgPlot.set_title('Image View')

        self.canvas.draw()


class FourFieldSingleFile(Gui):
    """
    Handles operations for four-field boxes stored as single, 3xZxZ numpy
    arrays.
    """

    def findPaths(self, pwd):
        """
        Get path to images.
        :params self: requires a `self` object from the Gui class (for
        QFileDialog)
        :params pwd: present working directory
        returns pathList: a list of the path to the single view arrays
        """
        msg = 'Select image folder'

        path = QtWidgets.QFileDialog.getExistingDirectory(self, msg, pwd)
        pathList = [path]
        self.ui.lineEdit.setText(pathList[0])
        return pathList

    def sortFiles(self, pathList, extension):
        """
        Search through the single directory path provided, then find and sort
        in alphabetical order a list of all image files saved as numpy arrays.
        :params pathList: a list whose only element is the path to a single
        directory containing the numpy arrays
        :params shortListIM: a sorted list with names of all found numpy image
        arrays. Does not include path or extension.
        """
        pathListIM = glob.glob(pathList[0] + '/*' + extension)

        # Filter out any that don't contain the optional keyword
        if self.keyword:
            pathListIM = [word for word in pathListIM if self.keyword in word]

        # Get a list of file names without paths, tags (like 'img'),
        # or extension
        shortListIM = [
            os.path.split(name)[-1].split(extension)[0] for name in pathListIM
        ]
        shortListIM.sort()

        return shortListIM

    def plot(self, baseName, extension):
        """
        Display AP, PA, and RL views in window.
        :params self: requires `self` object from Gui
        :params basename: file name ID (patient ID or other similar name that
        is shared among all files)
        """
        if self.keyword:
            self.filePath = self.pathList[0] + '/' + baseName + self.keyword + extension
        else:
            self.filePath = self.pathList[0] + '/' + baseName + extension

        arrayAP = np.load(self.filePath)[0]
        arrayPA = np.load(self.filePath)[1]
        arrayRL = np.load(self.filePath)[2]

        apPlot = self.figure.add_subplot(1, 3, 1)
        plt.imshow(arrayAP, cmap=self.cmap)
        plt.axis('off')
        plt.tight_layout()
        apPlot.set_title('AP View')

        paPlot = self.figure.add_subplot(1, 3, 2)
        plt.imshow(arrayPA, cmap=self.cmap)
        plt.axis('off')
        plt.tight_layout()
        paPlot.set_title('PA View')

        latPlot = self.figure.add_subplot(1, 3, 3)
        plt.imshow(arrayRL, cmap=self.cmap)
        plt.axis('off')
        plt.tight_layout()
        latPlot.set_title('Lateral View')

        self.canvas.draw()


def main():
    """
    Opens GUI
    """
    if system() == 'Windows':  # Display the icon in the taskbar
        if 'Vista' not in platform():
            myappid = 'numpyScan'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                myappid)
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    ex1 = Gui()
    ex1.setWindowTitle('NumpyScan')
    # ex1.setWindowIcon(QtGui.QIcon('ic_assessment_black_48dp_2x'))
    ex1.show()
    ex1.activateWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
