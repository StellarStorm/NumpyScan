λ conda create -n NumpyScan python=3 matplotlib=2 numpy
Solving environment: done

## Package Plan ##

  environment location: C:\Users\SGay1\AppData\Local\Continuum\miniconda3\envs\NumpyScan

  added / updated specs:
    - matplotlib=2
    - numpy
    - python=3


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    python-3.7.2               |       h8c8aaf0_0        17.7 MB
    blas-1.0                   |              mkl           6 KB
    vc-14.1                    |       h0510ff6_4           6 KB
    numpy-1.15.4               |   py37h19fb1c0_0          47 KB
    matplotlib-2.2.3           |   py37hd159220_0         6.5 MB
    ------------------------------------------------------------
                                           Total:        24.3 MB

The following NEW packages will be INSTALLED:

    blas:            1.0-mkl
    ca-certificates: 2018.03.07-0
    certifi:         2018.11.29-py37_0
    cycler:          0.10.0-py37_0
    freetype:        2.9.1-ha9979f8_1
    icc_rt:          2019.0.0-h0cc432a_1
    icu:             58.2-ha66f8fd_1
    intel-openmp:    2019.1-144
    jpeg:            9b-hb83a4c4_2
    kiwisolver:      1.0.1-py37h6538335_0
    libpng:          1.6.35-h2a8f88b_0
    matplotlib:      2.2.3-py37hd159220_0
    mkl:             2019.1-144
    mkl_fft:         1.0.6-py37h6288b17_0
    mkl_random:      1.0.2-py37h343c172_0
    numpy:           1.15.4-py37h19fb1c0_0
    numpy-base:      1.15.4-py37hc3f5095_0
    openssl:         1.1.1a-he774522_0
    pip:             18.1-py37_0
    pyparsing:       2.3.0-py37_0
    pyqt:            5.9.2-py37h6538335_2
    python:          3.7.2-h8c8aaf0_0
    python-dateutil: 2.7.5-py37_0
    pytz:            2018.7-py37_0
    qt:              5.9.7-vc14h73c81de_0
    setuptools:      40.6.3-py37_0
    sip:             4.19.8-py37h6538335_0
    six:             1.12.0-py37_0
    sqlite:          3.26.0-he774522_0
    tornado:         5.1.1-py37hfa6e2cd_0
    vc:              14.1-h0510ff6_4
    vs2015_runtime:  14.15.26706-h3a45250_0
    wheel:           0.32.3-py37_0
    wincertstore:    0.2-py37_0
    zlib:            1.2.11-h62dcd97_3
